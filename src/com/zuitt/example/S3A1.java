package com.zuitt.example;

import java.util.Scanner;

public class S3A1 {
    public static void main(String[] args){
        Scanner in = new Scanner(System.in);
        System.out.println("Input an integer whose factorial will be computed: ");

        int num = 0;
        try{
            num = in.nextInt();
        }
        //catch any errors
        catch (Exception e){
            System.out.println("Invalid Input!");
            e.printStackTrace();
        }
        finally {
            System.out.println("You have entered: " + num);
        }

        int x = 1;
        int y = 1;
       while(x <= num){
           y = x * y;
           x++;
        }
        System.out.println("The factorial of " + num + " is " + y);
    }
}
